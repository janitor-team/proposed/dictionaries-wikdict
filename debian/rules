#!/usr/bin/make -f
# export DH_VERBOSE=1

FREEDICT_TOOLS ?= /usr/share/freedict
export FREEDICT_TOOLS

# pass parallelisation through from debhelper
ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
    NUMJOBS = $(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
    MAKEFLAGS += -j$(NUMJOBS)
endif
DICTS=$(strip $(shell find . -maxdepth 1 -name '???-???'|sort |sed 's|^\./||g'|tr '\n' ' '))

# overwrite any existing definition
export FREEDICT_TOOLS = /usr/share/freedict

# If the environment variable BUILD_MODE=dummy is set, no dictionaries are
# build, but only dummy files are created. This is useful for test builds of the
# package, because the actual build takes ages.
BUILD_MODE ?= normal

%:
	dh $@

override_dh_auto_build: $(DICTS)

$(DICTS):
ifeq ($(BUILD_MODE),normal)
	$(MAKE) -C $@ build-dictd
else
	mkdir -p $@/build/dictd
	touch $@/build/dictd/$@.dict.dz $@/build/dictd/$@.index
endif

override_dh_auto_clean:
	dh_clean
	dh_testdir
	rm -f build-stamp timestamp configure-stamp
	rm -rf config.txt
	for DICT in $(DICTS); do \
		rm -rf $$DICT/build; \
		rm -rf debian/dict-freedict-$$DICT.dirs; \
		rm -rf debian/dict-freedict-$$DICT.docs; \
		rm -rf debian/dict-freedict-$$DICT.postinst; \
		rm -rf debian/dict-freedict-$$DICT.postrm; \
	done
	dh_clean


override_dh_auto_install:
	# create debian/dict-freedict-$LANG.dirs file and maint scripts
	for DICT in $(DICTS); do \
		sed "s/LANG/$$DICT/g" debian/dict-freedict-lang-dirs.tmpl > \
			debian/dict-freedict-$$DICT.dirs; \
		cp debian/postinst debian/dict-freedict-$$DICT.postinst; \
		cp debian/postrm debian/dict-freedict-$$DICT.postrm; \
	done

	dh_installdirs

	# copy files to final destination
	for DICT in $(DICTS); do \
		cp $$DICT/build/dictd/$$DICT.dict.dz debian/dict-freedict-$$DICT/usr/share/dictd/freedict-$$DICT.dict.dz; \
		cp $$DICT/build/dictd/$$DICT.index debian/dict-freedict-$$DICT/usr/share/dictd/freedict-$$DICT.index; \
	done

	# generate *.docs-files:
	sh debian/gendocs.sh
	dh_install

# this rule is explicitly here to document the source retrieval workflow
# requires python3
fetch-orig:
	python3 $(FREEDICT_TOOLS)/fetchdictdata.py --orig --criteria 'sourceURL:wikdict'

# generate debian/copyright and debian/control
gen-cc:
	python3 $(FREEDICT_TOOLS)/fetchdictdata.py --dc --criteria 'sourceURL:wikdict' \
		--no-desc-version

.PHONY: override_dh_auto_build_indep override_dh_auto_clean override_dh_auto_install $(DICTS)
